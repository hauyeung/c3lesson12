﻿namespace cshapr3_lesson12_1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.inputtextbox = new System.Windows.Forms.TextBox();
            this.patterntextbox = new System.Windows.Forms.TextBox();
            this.replacetextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.listbox = new System.Windows.Forms.ListBox();
            this.ignorecasecheckbox = new System.Windows.Forms.CheckBox();
            this.matchradiobutton = new System.Windows.Forms.RadioButton();
            this.replaceradiobutton = new System.Windows.Forms.RadioButton();
            this.evalbutton = new System.Windows.Forms.Button();
            this.exitbutton = new System.Windows.Forms.Button();
            this.resultlabel = new System.Windows.Forms.Label();
            this.errortextbox = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pattern";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Replace";
            // 
            // inputtextbox
            // 
            this.inputtextbox.Location = new System.Drawing.Point(88, 13);
            this.inputtextbox.Name = "inputtextbox";
            this.inputtextbox.Size = new System.Drawing.Size(378, 20);
            this.inputtextbox.TabIndex = 3;
            // 
            // patterntextbox
            // 
            this.patterntextbox.Location = new System.Drawing.Point(88, 41);
            this.patterntextbox.Name = "patterntextbox";
            this.patterntextbox.Size = new System.Drawing.Size(378, 20);
            this.patterntextbox.TabIndex = 4;
            // 
            // replacetextbox
            // 
            this.replacetextbox.Location = new System.Drawing.Point(88, 71);
            this.replacetextbox.Name = "replacetextbox";
            this.replacetextbox.Size = new System.Drawing.Size(378, 20);
            this.replacetextbox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Result";
            // 
            // listbox
            // 
            this.listbox.FormattingEnabled = true;
            this.listbox.Location = new System.Drawing.Point(88, 137);
            this.listbox.Name = "listbox";
            this.listbox.Size = new System.Drawing.Size(201, 277);
            this.listbox.TabIndex = 7;
            // 
            // ignorecasecheckbox
            // 
            this.ignorecasecheckbox.AutoSize = true;
            this.ignorecasecheckbox.Location = new System.Drawing.Point(307, 150);
            this.ignorecasecheckbox.Name = "ignorecasecheckbox";
            this.ignorecasecheckbox.Size = new System.Drawing.Size(83, 17);
            this.ignorecasecheckbox.TabIndex = 8;
            this.ignorecasecheckbox.Text = "Ignore Case";
            this.ignorecasecheckbox.UseVisualStyleBackColor = true;
            // 
            // matchradiobutton
            // 
            this.matchradiobutton.AutoSize = true;
            this.matchradiobutton.Location = new System.Drawing.Point(307, 111);
            this.matchradiobutton.Name = "matchradiobutton";
            this.matchradiobutton.Size = new System.Drawing.Size(55, 17);
            this.matchradiobutton.TabIndex = 9;
            this.matchradiobutton.TabStop = true;
            this.matchradiobutton.Text = "Match";
            this.matchradiobutton.UseVisualStyleBackColor = true;
            // 
            // replaceradiobutton
            // 
            this.replaceradiobutton.AutoSize = true;
            this.replaceradiobutton.Location = new System.Drawing.Point(399, 111);
            this.replaceradiobutton.Name = "replaceradiobutton";
            this.replaceradiobutton.Size = new System.Drawing.Size(65, 17);
            this.replaceradiobutton.TabIndex = 10;
            this.replaceradiobutton.TabStop = true;
            this.replaceradiobutton.Text = "Replace";
            this.replaceradiobutton.UseVisualStyleBackColor = true;
            // 
            // evalbutton
            // 
            this.evalbutton.Location = new System.Drawing.Point(307, 184);
            this.evalbutton.Name = "evalbutton";
            this.evalbutton.Size = new System.Drawing.Size(75, 23);
            this.evalbutton.TabIndex = 11;
            this.evalbutton.Text = "Evaluate";
            this.evalbutton.UseVisualStyleBackColor = true;
            this.evalbutton.Click += new System.EventHandler(this.evalbutton_Click);
            // 
            // exitbutton
            // 
            this.exitbutton.Location = new System.Drawing.Point(399, 418);
            this.exitbutton.Name = "exitbutton";
            this.exitbutton.Size = new System.Drawing.Size(75, 23);
            this.exitbutton.TabIndex = 12;
            this.exitbutton.Text = "Exit";
            this.exitbutton.UseVisualStyleBackColor = true;
            this.exitbutton.Click += new System.EventHandler(this.exitbutton_Click);
            // 
            // resultlabel
            // 
            this.resultlabel.AutoSize = true;
            this.resultlabel.Location = new System.Drawing.Point(88, 111);
            this.resultlabel.Name = "resultlabel";
            this.resultlabel.Size = new System.Drawing.Size(0, 13);
            this.resultlabel.TabIndex = 13;
            // 
            // errortextbox
            // 
            this.errortextbox.AutoSize = true;
            this.errortextbox.Location = new System.Drawing.Point(26, 111);
            this.errortextbox.Name = "errortextbox";
            this.errortextbox.Size = new System.Drawing.Size(16, 13);
            this.errortextbox.TabIndex = 14;
            this.errortextbox.Text = "...";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 453);
            this.Controls.Add(this.errortextbox);
            this.Controls.Add(this.resultlabel);
            this.Controls.Add(this.exitbutton);
            this.Controls.Add(this.evalbutton);
            this.Controls.Add(this.replaceradiobutton);
            this.Controls.Add(this.matchradiobutton);
            this.Controls.Add(this.ignorecasecheckbox);
            this.Controls.Add(this.listbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.replacetextbox);
            this.Controls.Add(this.patterntextbox);
            this.Controls.Add(this.inputtextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Regex Tester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox inputtextbox;
        private System.Windows.Forms.TextBox patterntextbox;
        private System.Windows.Forms.TextBox replacetextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listbox;
        private System.Windows.Forms.CheckBox ignorecasecheckbox;
        private System.Windows.Forms.RadioButton matchradiobutton;
        private System.Windows.Forms.RadioButton replaceradiobutton;
        private System.Windows.Forms.Button evalbutton;
        private System.Windows.Forms.Button exitbutton;
        private System.Windows.Forms.Label resultlabel;
        private System.Windows.Forms.Label errortextbox;
    }
}

