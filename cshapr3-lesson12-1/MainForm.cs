﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace cshapr3_lesson12_1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            errortextbox.ForeColor = System.Drawing.Color.Red;
        }

        private void evalbutton_Click(object sender, EventArgs e)
        {
            
            if (matchradiobutton.Checked)
            {
                if (ignorecasecheckbox.Checked)
                {
                    try
                    {
                        Regex regex = new Regex(patterntextbox.Text, RegexOptions.IgnoreCase);
                        var n = regex.Matches(inputtextbox.Text);
                        if (n.Count >0)
                        {
                            foreach (var x in n)
                            {
                                if (!String.IsNullOrEmpty(x.ToString()))
                                {
                                    listbox.Items.Add(x.ToString());
                                }
                            }
                        }
                        else
                        {
                            errortextbox.Text = "No match found.";
                        }
                    }
                    catch (ArgumentNullException)
                    {
                        errortextbox.Text ="Please enter text into the text boxes.";
                    }
                    catch (ArgumentException)
                    {
                        errortextbox.Text ="Invalid regex string.";
                    }

                }
                else
                {
                    try
                    {
                        Regex regex = new Regex(patterntextbox.Text);
                        var n  = regex.Matches(inputtextbox.Text);
                        if (n.Count > 0)
                        {
                            foreach (var x in n)
                            {
                                if (!String.IsNullOrEmpty(x.ToString()))
                                {
                                    listbox.Items.Add(x.ToString());
                                }
                            }
                        }
                        else
                        {
                            errortextbox.Text = "No match found.";
                        }

                    }
                    catch (ArgumentNullException)
                    {
                        errortextbox.Text ="Please enter text into the text boxes.";
                    }
                    catch (ArgumentException)
                    {
                        errortextbox.Text ="Invalid regex string.";
                    }
                }
            }
            else if (replaceradiobutton.Checked)
            {               
                if (ignorecasecheckbox.Checked)
                {
                     try
                     {
                        Regex regex = new Regex(patterntextbox.Text, RegexOptions.IgnoreCase);
                        string text = regex.Replace(inputtextbox.Text,replacetextbox.Text);
                        if (!String.IsNullOrEmpty(text))
                        {
                            listbox.Items.Add(text);
                        }
                        else
                        {
                            errortextbox.Text ="Cannot replace string.";
                        }
                     }
                     catch (ArgumentNullException)
                     {
                         errortextbox.Text ="Please enter text into the text boxes.";
                     }
                     catch (ArgumentException)
                     {
                         errortextbox.Text ="Invalid regex string.";
                     }
                }
                else
                {
                    try
                    {
                        Regex regex = new Regex(patterntextbox.Text);
                        string text = regex.Replace(inputtextbox.Text, replacetextbox.Text);
                        if (!String.IsNullOrEmpty(text))
                        {
                            listbox.Items.Add(text);
                        }
                        else
                        {
                            errortextbox.Text ="Cannot replace string.";
                        }
                     
                    }
                    catch (ArgumentNullException)
                    {
                        errortextbox.Text ="Please enter text into the text boxes.";
                    }
                    catch (ArgumentException)
                    {
                        errortextbox.Text ="Invalid regex string.";
                    }
                }
            }
        }

        private void exitbutton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
